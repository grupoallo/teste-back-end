![GrupoAllo](https://allopagfacil.com.br/site-2020/img/logo-grupoallo.png)

# Teste prático - Backend

Olá candidato(a),

Agradecemos seu interesse em se juntar à nossa equipe! Como parte do processo de seleção, gostaríamos que você realizasse este teste prático para avaliarmos suas habilidades em desenvolvimento de APIs em Laravel com MySQL.

### Contexto
Um cliente precisa de uma API para gerenciar uma lista de tarefas a serem realizadas. Cada tarefa deve ter um título, uma descrição, uma data de criação e uma data de conclusão. O cliente também precisa que a API possa retornar uma lista de todas as tarefas, bem como uma tarefa específica pelo seu ID.

### Requisitos
- A API deve ser desenvolvida em Laravel.
- Os dados devem ser armazenados em um banco de dados MySQL.
- A API deve permitir criar, atualizar, excluir e listar tarefas.
- A API deve ter autenticação por token JWT.

### O que esperamos de você
- Crie um repositório no GitHub para o projeto.
- Desenvolva a API de acordo com os requisitos especificados acima.
- Implemente os testes unitários e garanta que todos eles passem.
- Escreva um README.md detalhado explicando como executar a API localmente e como executar os testes.
- Faça commits regulares e escreva mensagens de commit descritivas.
- Adote boas práticas de codificação e design de software.

### O que avaliaremos
- Qualidade do código e da arquitetura.
- Adesão aos requisitos do cliente.
- Boas práticas de codificação.
- Capacidade de documentação.

### Como enviar sua solução
- Faça um fork deste repositório ou crie um em sua conta do GitLab.
- Desenvolva a solução no seu repositório criado.
- Envie-nos o link do repositório que deve estar como público.

### Informações adicionais
- Se você tiver alguma dúvida sobre o teste ou o contexto do projeto, não hesite em entrar em contato conosco.
- Não se preocupe em criar uma interface do usuário para a API. Apenas crie a API.
- Não se preocupe em implementar outras funcionalidades além das especificadas nos requisitos.

Boa sorte!